<!-- 
 Copyright 2015, Association pour la Culture numérique Libre - CLibre
 (http://clibre.tn - contact@clibre.tn)
 Licence :
 Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
 modifier suivant les termes de la GNU General Public License telle que 
 publiée par la Free Software Foundation ; soit la version 3 de la licence,
 soit (à votre gré) toute version ultérieure.
 Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE
 GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION
 à UN BUT PARTICULIER. Consultez la GNU General Public License pour 
 plus de détails.
 Vous devez avoir reçu une copie de la GNU General Public License en même 
 temps que ce programme ; si ce n'est pas le cas, consultez 
 <http://www.gnu.org/licenses>
-->

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Taxe sur les constructions</title>
<script type="text/javascript">
	/***
 	* Fonction CompterServices() :
 	*	 Compte le nombre de services sélectionnés par l'utilisateur.
 	***/
 	function CompterServices() {
 	 	var checkboxElems     = document.getElementsByName("services");
 		var nbrServiceTxt     = document.getElementById("nbr_services");
 		var autreServicesElem = document.getElementById("autre_services");
 		var pourcentageElem   = document.getElementById("pourcentage");
 		var btnCalculer       = document.getElementById("calculer");
 		var nbrService = 0;

		// Calculer le nombre de services sélectonnés.
		for (var i=0; i<checkboxElems.length; i++) {       
			if (checkboxElems[i].checked == true){
 	        	nbrService++;
 	        }
 	    }
  	    nbrServiceTxt.value = nbrService;

		// Le calcul de la taxe n'est possible que si au moins un service
		// est sélectionné.  	       
 	    if (nbrService > 0 ) {
 	    	btnCalculer.disabled  = false;
 	    }
 	    else {
 	       	btnCalculer.disabled  = true;
 	       	pourcentageElem.value = "" ;
 	       	nbrServiceTxt.value   = "" ;
 	    }

		// Le choix d'un autre service n'est possible que si 4 sont déjà
		// sélectionnés.
 	    if (nbrService < 5 ) {
 	       	autreServicesElem.disabled = true;
 	       	autreServicesElem.checked  = false;
	    }
	    else {
	       	autreServicesElem.disabled = false;
	    }

	    // Le pourcentage appliqué sur les services.
	    MajPourcentage (nbrService);
	}
	
 	/***
 	 * Fonction MajPourcentage(nbrService) :
 	 *	 Détermine le pourcentage à appliquer selon le nombre de services
 	 *   sélectionnés par l'utilisateur. 
 	 ***/
	function MajPourcentage (nbrService) {
		var pourcentageElem   = document.getElementById("pourcentage");
 		var autreServicesElem = document.getElementById("autre_services");
		switch (nbrService) {
       	case 1 :
       	case 2 :
       		pourcentageElem.value = 8 ;
       		break;
       	case 3 :
       	case 4 :
       		pourcentageElem.value = 10 ;
       		break;
       	case 5 :
       	case 6 :
        	if (autreServicesElem.checked == false) {
        		pourcentageElem.value = 12 ;
        	}
        	else {
        		pourcentageElem.value = 14 ;
        	}
       		break;
		}
	}
 	
 	/***
 	 * Fonction MajPrixM2() :
 	 *	 Détermine le prix de base du mètre carré selon la surface couverte
 	 *   donnée.
 	 *	 Le prix varie d'une municipalité à une autre.
 	 *   Les valeurs par défaut sont celles de la municipalité de Sayada. 
 	 ***/
	function MajPrixM2() {
 		var surfaceElem = document.getElementById("surface_m2");
 		var prixElem	= document.getElementById("prix_m2");
 		var prix_m2;
 		if ( surfaceElem.value <= 100) {
			prix_m2 = 150;
		}
		else {
 			if (surfaceElem.value <= 200) {
				prix_m2 = 200;
			}
			else {
				if (surfaceElem.value <= 400) {
					prix_m2 = 250;
				}
				else {
 					prix_m2 = 300;
				}
			}
		}
 		prixElem.value = prix_m2;
	}
</script>
</head>
<body>
	<h3>Taxe sur construction</h3>
<?php
if (! isset ( $_POST ['calculer'] )) {
	$surfaceCouverteM2 = "";
	$prixRefM2 = "";
	$taxeBase = "";
	$nbrServices = "";
	$pourcentage = "";
	$pourcentageHabitat = "4";
	$taxe = "";
} else {
	if (preg_match ( "#^[0-9]+$#", $_POST ['surface_m2'] )) {
		$surfaceCouverteM2 = ($_POST ['surface_m2']);
		$prixRefM2 = ($_POST ['prix_m2']);
		$taxeBase = (2 * $surfaceCouverteM2 * $prixRefM2) / 100;
		$nbrServices = ($_POST ['nbr_services']);
		$pourcentage = ($_POST ['pourcentage']);
		$pourcentageHabitat = "4";
		$taxe = ($taxeBase * ($pourcentage + $pourcentageHabitat)) / 100;
	} else {
		$surfaceCouverteM2 = "";
		$prixRefM2 = "";
		$taxeBase = "";
		$taxe = "";
	}
}
?>
<form action="" id="form" method="post">
		<label>Surface couverte (m²) :</label><br />
		<input id="surface_m2" maxlength="150" name="surface_m2" 
			onchange="MajPrixM2();" required="true" type="text" 
			value="<?php echo "$surfaceCouverteM2" ?>" /><br />
		<small>Donner la surface couverte en m².</small><br />
		<br />
		<fieldset>
			<legend>Liste des services :</legend>
			<input name="services" onclick="CompterServices();" type="checkbox"
				value="s1" /> Ramassage d'ordures<br /> 
			<input name="services" onclick="CompterServices();" type="checkbox"
				value="s2" /> Éclairage public<br />
			<input name="services" onclick="CompterServices();" type="checkbox"
				value="s3" /> Routes goudronnées<br /> 
			<input name="services" onclick="CompterServices();" type="checkbox"
				value="s4" /> Trottoirs aménagées<br /> 
			<input name="services" onclick="CompterServices();" type="checkbox"
				value="s5" /> Évacuation des eaux usées<br /> 
			<input name="services" onclick="CompterServices();" type="checkbox"
				value="s6" /> Évacuation des eaux pluviales<br /> 
			<input disabled="disabled" id="autre_services" name="autre_services"
				onclick="CompterServices();" type="checkbox" 
				value="sa" /> Autres services
		</fieldset>
		<p>
			<input disabled="disabled" id="calculer" name="calculer"
				type="submit" value="Calculer" />
		</p>
		<p>
			<label>Prix par m² :</label><br />
			<input id="prix_m2" maxlength="150" name="prix_m2" 
				readonly="readonly" type="text" 
				value="<?php echo "$prixRefM2 TND" ?>" /><br />
			<small>Prix de référence déterminé depuis la surface couverte.</small>
		</p>
		<p>
			<label>Taxe de base :</label><br />
			<input maxlength="150" name="taxe_base" readonly="readonly"
				type="text" value="<?php echo "$taxeBase TND" ?>" /><br />
			<small>Taxe calculée à partir de la surface en m² et le prix
				de référence par m².</small>
		</p>
		<p>
			<label>Nombre de services :</label><br />
			<input id="nbr_services" maxlength="150" name="nbr_services" 
				readonly="readonly" type="text" 
				value="<?php echo "$nbrServices" ?>" /><br /> 
			<small>Nombre de services disponibles.</small>
		</p>
		<p>
			<label>Pourcentage :</label><br />
			<input id="pourcentage" maxlength="150"
				name="pourcentage" readonly="readonly" type="text"
				value="<?php echo "$pourcentage %" ?>" /><br /> 
			<small>Pourcentage calculé selon le nombre de services.</small>
		</p>
		<p>
			<label>Pourcentage amélioration d'habitat :</label><br />
			<input id="pHabitat" maxlength="150" name="pHabitat" readonly="readonly"
				type="text" value="<?php echo "$pourcentageHabitat %" ?>" /><br /> 
			<small>Pourcentage d'amélioration d'habitat.</small>
		</p>
		<p>
			<label><font color="blue" size="+2">Taxe à payer :</font></label><br />
			<input maxlength="150" name="taxe" readonly="readonly" type="text"
				value="<?php echo "$taxe TND" ?>" /><br /> 
			<small>Taxe à payer.</small>
		</p>
	</form>
	<br />
	<hr />
	<p align="center">
		Formulaire de calcul offert par l'<a href="http://clibre.tn" 
		target="_blank">Association pour la Culture numérique Libre - 
		CLibre</a><br /> Pour vos remarques est suggestions vous pouvez <a
		href="http://clibre.tn/contact/" target="_blank">nous contacter</a>.
	</p>

</body>
</html>