<!-- 
 Copyright 2015, Association pour la Culture numérique Libre - CLibre
 (http://clibre.tn - contact@clibre.tn)
 Licence :
 Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
 modifier suivant les termes de la GNU General Public License telle que 
 publiée par la Free Software Foundation ; soit la version 3 de la licence,
 soit (à votre gré) toute version ultérieure.
 Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE
 GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION
 à UN BUT PARTICULIER. Consultez la GNU General Public License pour 
 plus de détails.
 Vous devez avoir reçu une copie de la GNU General Public License en même 
 temps que ce programme ; si ce n'est pas le cas, consultez 
 <http://www.gnu.org/licenses>
-->

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>احتساب معلوم رخصة البناء</title>
<script type="text/javascript">
 	
 	/***
 	 * Fonction MajPrix() :
 	 *	 Détermine le prix de base du mètre carré selon la surface couverte
 	 *   donnée.
 	 *	 Le prix varie d'une municipalité à une autre.
 	 *   Les valeurs par défaut sont celles de la municipalité de Sayada. 
 	 ***/
	function MajPrix() {
 		var surfaceElem = document.getElementById("surface_m2");
 		var taxeFixeElem = document.getElementById("taxe_fixe");
 		var prixElem	= document.getElementById("prix_suppl_m2");
 		var btnCalculer       = document.getElementById("calculer");
		var surfaceExplElem = document.getElementById("surface_exploitee");
		var dureeElem = document.getElementById("duree_exploitation");

 		if ( surfaceElem.value <= 100) {
			taxeFixeElem.value = 15;
			prixElem.value = 0.1;
			surfaceExplElem.value = 8; 
			dureeElem.value = 20;
		}
		else {
 			if (surfaceElem.value <= 200) {
				taxeFixeElem.value = 60;
				prixElem.value = 0.3;
				surfaceExplElem.value = 10; 
				dureeElem.value = 25;
			}
			else {
				if (surfaceElem.value <= 300) {
					taxeFixeElem.value = 120;
					prixElem.value = 0.4;
					surfaceExplElem.value = 12; 
					dureeElem.value = 30;
				}
				else {
					if (surfaceElem.value <= 400) {
						taxeFixeElem.value = 300;
						prixElem.value = 0.6;
						surfaceExplElem.value = 15; 
						dureeElem.value = 40;
					}
					else {
 						taxeFixeElem.value = 750;
 						prixElem.value = 1;
 						if (surfaceElem.value <= 500) {
 							surfaceExplElem.value = 16; 
							dureeElem.value = 60;
 						}
 						else {
 							if (surfaceElem.value <= 600) {
 								surfaceExplElem.value = 18; 
								dureeElem.value = 100;
 							}
 							else {
 								if (surfaceElem.value <= 700) {
 									surfaceExplElem.value = 20; 
									dureeElem.value = 120;
 								}
 								else {
 									surfaceExplElem.value = 25; 
									dureeElem.value = 140;
 								}
 							}
 						}
 					}
				}
			}
		}
	}
</script>
</head>
<body>
	<h3 DIR="RTL" align="center">احتساب معلوم رخصة البناء</h3>
<?php
	if (preg_match ( "#^[0-9]+$#", $_POST ['surface_m2'] )) {
		$surfaceCouverteM2 = ($_POST ['surface_m2']);
		$taxeFixe = ($_POST ['taxe_fixe']);
		$prixSupplM2 = ($_POST ['prix_suppl_m2']);
		$taxePermis = $taxeFixe + ($surfaceCouverteM2 * $prixSupplM2);
		$prixOccupationVoieM2 = "1";
		$surfaceExploitee = ($_POST ['surface_exploitee']);
		$dureeMinExploitation = ($_POST ['duree_exploitation']);
		$taxOccupationVoie = $prixOccupationVoieM2 * $surfaceExploitee * $dureeMinExploitation;
		$taxe = ($taxePermis +  $taxOccupationVoie);
	} else {
		$surfaceCouverteM2 = "";
		$taxeFixe = "";
		$prixSupplM2 = "";
		$taxePermis = "";
		$prixOccupationVoieM2 = "1";
		$surfaceExploitee = "";
		$dureeMinExploitation = "";
		$taxOccupationVoie = "";
		$taxe = "";
	}

?>
<form action="" id="form" method="post">
		<p DIR="RTL">
		<label>المساحة المغطاة (م2) :</label><br />
		<input id="surface_m2" maxlength="150" name="surface_m2" 
			onchange="MajPrix();" required="true" type="text" 
			value="<?php echo "$surfaceCouverteM2" ?>" /><br />
		<small>أدخل المساحة المغطاة بالمتر المربع. </small><br />
		</p>
		<p DIR="RTL">
			<input id="calculer" name="calculer"
				type="submit" value="احسب" />
		</p>
		<p DIR="RTL">
			<label>معلوم قار :</label><br />
			<input id="taxe_fixe" maxlength="150" name="taxe_fixe" readonly="readonly"
				type="text" value="<?php echo "$taxeFixe دت" ?>" /><br />
			<small>معلوم قار حسب المساحة المغطاة بالمتر المربع.</small>
		</p>
		<p DIR="RTL">
			<label>معلوم إضافي (م2) :</label><br />
			<input id="prix_suppl_m2" maxlength="150" name="prix_suppl_m2" 
				readonly="readonly" type="text" 
				value="<?php echo "$prixSupplM2 دت" ?>" /><br />
			<small>معلوم إضافي حسب المساحة المغطاة بالمتر المربع.</small>
		</p>
		<p DIR="RTL">
			<label>معلوم رخصة البناء :</label><br />
			<input id="taxe_permis" maxlength="150" name="taxe_permis" 
				readonly="readonly" type="text" 
				value="<?php echo "$taxePermis دت" ?>" /><br />
			<small>معلوم رخصة البناء.</small>
		</p>
		<p DIR="RTL">
			<label><font size="+1">استغلال الطريق العام :</font></label><br />
		</p>
 		<p DIR="RTL">
			<label>ثمن المتر المربع :</label><br />
			<input id="prix_voie_m2" maxlength="150" name="prix_voie_m2" 
				readonly="readonly" type="text" 
				value="<?php echo "$prixOccupationVoieM2 دت" ?>" /><br />
			<small>ثمن استغلال المتر المربع.</small>
		</p>
		<p DIR="RTL">
			<label>مساحة الاستغلال (م2):</label><br />
			<input id="surface_exploitee" maxlength="150" name="surface_exploitee" 
				readonly="readonly" type="text" 
				value="<?php echo "$surfaceExploitee م2" ?>" /><br />
			<small>المساحة المستغلة من الطريق العام.</small>
		</p>
		<p DIR="RTL">
			<label>مدة الإشغال الدنيا :</label><br />
			<input id="duree_exploitation" maxlength="150" name="duree_exploitation" 
				readonly="readonly" type="text" 
				value="<?php echo "$dureeMinExploitation يوم" ?>" /><br />
			<small>مدة الإشغال الدنيا للطريق العام بالأيام.</small>
		</p>
		<p DIR="RTL">
			<label>معلوم استغلال الطريق العام :</label><br />
			<input id="tax_occupation_voie" maxlength="150" name="tax_occupation_voie" 
				readonly="readonly" type="text" 
				value="<?php echo "$taxOccupationVoie دت" ?>" /><br />
			<small>معلوم استغلال الطريق العام.</small>
		</p>
		<p DIR="RTL">
			<label><font color="blue" size="+2">المعلوم الذي يجب دفعه :</font></label><br />
			<input maxlength="150" name="taxe" readonly="readonly" type="text"
				value="<?php echo "$taxe دت" ?>" /><br /> 
			<small>المعلوم الذي يجب دفعه.</small>
		</p>
		
	</form>
	<br />
	<hr />
	<p DIR="RTL" align="center">
		ورقة حساب مهداة من <a href="http://clibre.tn" 
		target="_blank"> جمعية الثقافة الرقمية الحرة - CLibre </a><br /> يمكنكم <a
		href="http://clibre.tn/contact/" target="_blank">الإتصال بنا</a> لإرسال مقترحاتكم أو أسئلنكم.
	</p>

</body>
</html>
