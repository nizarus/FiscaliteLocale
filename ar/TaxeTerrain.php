<!-- 
 Copyright 2015, Association pour la Culture numérique Libre - CLibre
 (http://clibre.tn - contact@clibre.tn)
 Licence :
 Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
 modifier suivant les termes de la GNU General Public License telle que 
 publiée par la Free Software Foundation ; soit la version 3 de la licence,
 soit (à votre gré) toute version ultérieure.
 Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE
 GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION
 à UN BUT PARTICULIER. Consultez la GNU General Public License pour 
 plus de détails.
 Vous devez avoir reçu une copie de la GNU General Public License en même 
 temps que ce programme ; si ce n'est pas le cas, consultez 
 <http://www.gnu.org/licenses>
-->

<!Doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>احتساب المعلوم على العقارات الغير المبنية</title>

<script type="text/javascript">
	/***
	 * Fonction MajPrixM2Terrain() :
	 *	 Détermine le prix de base du mètre carré selon la zone donnée.
	 ***/
 		function MajPrixM2Terrain() {
 			var radioZone1  = document.getElementById("zone1");
 			var radioZone2  = document.getElementById("zone2");
 			var btnCalculer = document.getElementById("calculer");
 	 		var prixElem 	= document.getElementById("prix_m2");
 			

 			// Déterminer le prix par m2.
            if (radioZone1.checked == true){
                btnCalculer.disabled = false;
                prixElem.value = "0.318";
            }

            if (radioZone2.checked == true){
                btnCalculer.disabled = false;
                prixElem.value = "0.095";
            }
 		}
    </script>
</head>
<body>
	<h3 dir="RTL" align="center">احتساب المعلوم على العقارات الغير المبنية</h3>
	<?php
	if (! isset ( $_POST ['calculer'] )) {
		$surfaceTerrainM2 = "";
		$prixM2 = "";
		$taxe = "";
	} else {
		if (preg_match ( "#^[0-9]+$#", $_POST ['surface_m2'] )) {
			$surfaceTerrainM2 = ($_POST ['surface_m2']);
			$prixM2 = ($_POST ['prix_m2']);
			$taxe = ($prixM2 * $surfaceTerrainM2);
		}
	}
	
	?>
	<form id="form" method="post" action="">
		<p DIR="RTL">
		<label>المساحة (م2) :</label><br />
		<input required id="surface_m2" name="surface_m2" type="text"
			maxlength="150" value="<?php echo "$surfaceTerrainM2" ?>" /><br />
		<small>أدخل المساحة بالمتر المربع.</small><br />
		</p>
		<br />
		<fieldset><p DIR="RTL">
			<legend>موقع العقار :</legend>
			<input id="zone1" name="zone" type="radio" value="z1" 
				onchange="MajPrixM2Terrain();" /> المنطقة الأولى. <br />
			<input id="zone2" name="zone" type="radio" value="z2"
				onchange="MajPrixM2Terrain();" /> المنطقة الثانية.
		</p></fieldset>
		<p DIR="RTL">
			<input disabled type="submit" id="calculer" name="calculer" 
				value="احسب" />
		</p>
		<p DIR="RTL">
			<label>الثمن المرجعي (م2) :</label><br />
			<input readonly id="prix_m2" name="prix_m2" type="text" 
				maxlength="150" value="<?php echo "$prixM2 دت" ?>" /><br />
			<small>الثمن المرجعي للمتر المربع حسب المنطقة.</small>
		</p>
		<p DIR="RTL">
			<label><font size="+2" color="blue">المعلوم الذي يجب دفعه :</font></label><br />
			<input readonly name="taxe" type="text" maxlength="150"
				value="<?php echo "$taxe دت" ?>" /><br />
			<small>المعلوم الذي يجب دفعه.</small>
		</p>
	</form>
	<br />
	<hr />
	<p DIR="RTL" align="center">
		ورقة حساب مهداة من <a href="http://clibre.tn" 
		target="_blank"> جمعية الثقافة الرقمية الحرة - CLibre </a><br /> يمكنكم <a
		href="http://clibre.tn/contact/" target="_blank">الإتصال بنا</a> لإرسال مقترحاتكم أو أسئلنكم.
	</p>

</body>
</html>
